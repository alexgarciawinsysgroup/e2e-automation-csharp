﻿using e2e_automation.pages.backoffice;
using e2e_automation.tools;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace e2e_automation.steps.backoffice
{
    [Binding]
    public sealed class bLoginSteps
    {
        private readonly ScenarioContext context;
        
        bLoginPage bLoginPage;
        bDashboardPage bDashboard;

        public bLoginSteps(ScenarioContext injectedContext)
        {
            context = injectedContext;
            Globals.bDriver = new ChromeDriver();
            Globals.bDriver.Manage().Window.Maximize();
            Globals.bDriver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(15);
            Globals.bDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);
            bLoginPage = new bLoginPage();
            bDashboard = new bDashboardPage();
        }

        [Given("the backoffice login screen")]
        public void TheBackofficeLoginScreen()
        {
            bLoginPage.openPage();
        }

        [When("I log to backoffice with user '(.*)' and password '(.*)'")]
        public void ILogInToBackoffice(string pUser, string pPassword)
        {
            bLoginPage.login(pUser, pPassword);            
        }

        //[Given(@"I am logged in")]
        [Then("the backoffice dashboard its shown")]
        public void TheBackofficeDashboardItsShown()
        {
            Assert.IsTrue(bDashboard.isVisible());
        }

    }
}
