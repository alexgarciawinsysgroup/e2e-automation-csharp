﻿using OpenQA.Selenium;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using e2e_automation.pages.backoffice.common;

namespace e2e_automation.steps.backoffice.common
{
    [Binding]
    public sealed class bDialogSteps
    {
        private readonly ScenarioContext context;
        bDialogPage bDialogPage;

        public bDialogSteps(ScenarioContext injectedContext)
        {
            context = injectedContext;
            bDialogPage = new bDialogPage();
        }

        //Available values for pType: error, warning, question, info, success
                                 
        [When(@"(.*) dialog appears")]
        public void WhenDialogAppears(string pType)
        {
            Assert.IsTrue(bDialogPage.isDialogShown());
            Assert.AreEqual(pType, bDialogPage.getDialogType());
        }

        [When(@"I click on Continue button from dialog")]
        public void WhenIClickOnContinueButtonFromDialog()
        {
            bDialogPage.clickContinue();
        }


        [Then(@"A (.*) alert which says '(.*)' appears")]
        public void TheAnAlertWhichSaysAppears(string pAlertColor, string pAlertMessage)
        {
            List<string> alertData = bDialogPage.isAlertMessageCorrect(pAlertColor, pAlertMessage);
            if (alertData == null)
            {
                Assert.Fail("Message not shown");
            }
            else
            {
                Assert.AreEqual(pAlertColor, alertData[0]);
                Assert.AreEqual(pAlertMessage, alertData[1]);
            }
        }
    }
}
