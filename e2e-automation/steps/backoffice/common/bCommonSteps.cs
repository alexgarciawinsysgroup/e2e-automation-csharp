﻿using e2e_automation.pages.backoffice.common;
using e2e_automation.pages.backoffice.consumables;
using OpenQA.Selenium;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using e2e_automation.tools;

namespace e2e_automation.steps.common
{
    [Binding]
    public sealed class bCommonSteps
    {
        private readonly ScenarioContext context;
        bCommonPage bCommonPage;

        public bCommonSteps(ScenarioContext injectedContext)
        {
            context = injectedContext;
            bCommonPage = new bCommonPage();
        }

        [Given(@"I acces to '(.*)'")]
        public void GivenIAccesTo(string pMenuItem)
        {
            goToMenu(pMenuItem);
        }


        [When(@"I press Add button")]
        public void WhenIPressAddButton()
        {
            bCommonPage.clickAdd();
        }

        [When(@"I click on Save button")]
        public void WhenIClickOnButton()
        {
            bCommonPage.clickSave();
            System.Threading.Thread.Sleep(1500);
        }


        [When(@"I fill '(.*)' field with '(.*)' value")]
        public void WhenIFillFieldWithValue(string pFieldId, string pValueId)
        {
            IWebElement field = bCommonPage.fillField(pFieldId, pValueId);
            Assert.AreEqual(pValueId, field.GetAttribute("value"));
        }

        [Then(@"The following row is shown")]
        public void ThenTheFollowingRowIsShown(Table pTable)
        {
            if (pTable != null)
            {
                List<Row> expectedTableValues = getParamTable(pTable);
                List<Row> actualTableValue = bCommonPage.getRows();


                foreach (Row expectedList in expectedTableValues)
                {
                    bool listShown = false;
                    foreach (Row actualList in actualTableValue)
                    {
                        if (bCommonPage.isExpectedRow(expectedList, actualList))
                        {
                            listShown = true;
                            break;
                        }
                    }
                    Assert.IsTrue(listShown, "The table value(s) (header#value) '" + expectedList.ToString() + "' is/are shown.");
                }
            }
            else
            {
                Assert.Fail("The data table its empty! Fill it with data!");
            }
        }

        [Then(@"The following row is not shown")]
        public void ThenTheFollowingRowIsNotShown(Table pTable)
        {
            if (pTable != null)
            {
                List<Row> expectedTableValues = getParamTable(pTable);
                List<Row> actualTableValue = bCommonPage.getRows();


                foreach (Row expectedList in expectedTableValues)
                {
                    bool listShown = false;
                    foreach (Row actualList in actualTableValue)
                    {
                        if (bCommonPage.isExpectedRow(expectedList, actualList))
                        {
                            listShown = true;
                            break;
                        }
                    }
                    Assert.IsFalse(listShown, "The table value(s) (header#value) '" + expectedList.ToString() + "' is/are NOT shown.");
                }
            }
            else
            {
                Assert.Fail("The data table its empty! Fill it with data!");
            }
        }

        [When(@"I edit the following row")]
        public void WhenIEditTheFollowingRow(Table pTable)
        {
            if (pTable != null)
            {
                List<Row> expectedTableValues = getParamTable(pTable);
                List<Row> actualTableValue = bCommonPage.getRows();

                    Row rowShown = null;
                    foreach (Row actualList in actualTableValue)
                    {
                        if (bCommonPage.isExpectedRow(expectedTableValues[0], actualList))
                        {
                            rowShown = actualList;
                            break;
                        }
                    }
                    if (rowShown != null)
                    {
                        bCommonPage.clickEditRow(rowShown.getActionsCell().element);
                    }
            }
            else
            {
                Assert.Fail("The data table its empty! Fill it with data!");
            }
        }

        [When(@"I delete the following row")]
        public void WhenIDeleteTheFollowingRow(Table pTable)
        {
            if (pTable != null)
            {
                List<Row> expectedTableValues = getParamTable(pTable);
                List<Row> actualTableValue = bCommonPage.getRows();

                Row rowShown = null;
                foreach (Row actualList in actualTableValue)
                {
                    if (bCommonPage.isExpectedRow(expectedTableValues[0], actualList))
                    {
                        rowShown = actualList;
                        break;
                    }
                }
                if (rowShown != null)
                {
                    bCommonPage.clickDeleteRow(rowShown.getActionsCell().element);
                }
            }
            else
            {
                Assert.Fail("The data table its empty! Fill it with data!");
            }
        }


       



        //TODO Implement this function/switch without Hardcoded values (dictionary?)
        private void goToMenu(string pMenuItem)
        {
            switch (pMenuItem)
            {
                case "Categories":
                    new bCategoriesPage().goTo();
                    break;

            }
        }

        //private List<List<string>> getParamTableData(Table pTable)
        //{
        //    List<List<string>> tableValues = new List<List<string>>(); ;
            
        //    foreach (TableRow row in pTable.Rows)
        //    {
        //        List<string> currRow = new List<string>();
        //        List<string> currHeader = new List<string>();
        //        foreach (String key in row.Keys)
        //        {
        //            currHeader.Add(key);
        //        }
        //        foreach (String value in row.Values)
        //        {
        //            currRow.Add(value);
        //        }

        //        for (int i = 0; i < currRow.Count; i++)
        //        {
        //            currRow[i] = currHeader[i] + "#" + currRow[i];
        //        }
        //        tableValues.Add(currRow);
        //    }

        //    return tableValues;
        //}
        private List<Row> getParamTable(Table pTable)
        {
            List<Row> tableValues = new List<Row>(); ;

            foreach (TableRow row in pTable.Rows)
            {
                List<string> currRow = new List<string>();
                List<string> currHeader = new List<string>();
                foreach (String key in row.Keys)
                {
                    currHeader.Add(key);
                }
                foreach (String value in row.Values)
                {
                    currRow.Add(value);
                }

                Row tableRow = new Row();
                for (int i = 0; i < currRow.Count; i++)
                {
                    tableRow.setCell(new RowCell(currHeader[i], currRow[i]));
                }
                tableValues.Add(tableRow);
            }

            return tableValues;
        }

    }
}
