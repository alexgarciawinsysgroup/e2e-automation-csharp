﻿using e2e_automation.pages.terminal;
using e2e_automation.tools;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace e2e_automation.steps.terminal
{
    [Binding]
    public sealed class tLoginSteps
    {
        private readonly ScenarioContext context;
        tLoginPage tLoginPage;
        tDashboardPage tDashboardPage;

        public tLoginSteps(ScenarioContext injectedContext)
        {
            Globals.tDriver = new ChromeDriver();
            Globals.tDriver.Manage().Window.Maximize();
            Globals.tDriver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(15);
            Globals.tDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);
            tLoginPage = new tLoginPage();
            tDashboardPage = new tDashboardPage();
        }

        [Given("the terminal login screen")]
        public void TheTerminalLoginScreen()
        {
            tLoginPage.openPage();
        }

        [When("I log to terminal with user '(.*)' and password '(.*)'")]
        public void ILogInToTerminal(string pUser, string pPassword)
        {
            tLoginPage.login(pUser, pPassword);
        }

        [Then(@"the terminal dashboard its shown")]
        public void ThenTheTerminalDashboardItsShown()
        {
            Assert.IsTrue(tDashboardPage.isVisible());
        }

    }
}
