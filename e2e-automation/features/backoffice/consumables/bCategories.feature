﻿Feature: bCategories
	In order to have consumable categories
	As a backoffice user
	I want to create, edit and delete a category

#Background:
#	Given I am logged in
#	And I acces to 'Categories'

Scenario Outline: 01 Create a category
	Given I acces to 'Categories'
	When I press Add button
	And I fill 'Name' field with '<NAME>' value
	And I fill 'Description' field with '<DESCRIPTION>' value
	And I click on Save button
	Then A green alert which says 'Operation completed sucessfully' appears
	And The following row is shown
		| Category Name | Type   |
		| <NAME>        | <TYPE> |

	Examples:
		| NAME        | DESCRIPTION          | TYPE   |
		| Magic paper | For hand magic games | Custom |

Scenario Outline: 02 Edit a category
	Given I acces to 'Categories'
	When I edit the following row
		| Category Name |
		| <OLD_NAME>    |
	And I fill 'Name' field with '<NEW_NAME>' value
	And I fill 'Description' field with '<NEW_DESCRIPTION>' value
	And I click on Save button
	Then A green alert which says 'Operation completed sucessfully' appears
	And The following row is shown
		| Category Name | Type   |
		| <NEW_NAME>    | <TYPE> |

	Examples:
		| OLD_NAME    | OLD_DESCRIPTION      | NEW_NAME   | NEW_DESCRIPTION  | TYPE   |
		| Magic paper | For hand magic games | Fake paper | For joking games | Custom |

Scenario Outline: 03 Delete a category
	Given I acces to 'Categories'
	When I delete the following row
		| Category Name |
		| <NAME>        |
	And warning dialog appears
	And I click on Continue button from dialog
	Then A green alert which says 'Operation completed sucessfully' appears
	And The following row is not shown
		| Category Name |
		| <NAME>        |

	Examples:
		| NAME       |
		| Fake paper |