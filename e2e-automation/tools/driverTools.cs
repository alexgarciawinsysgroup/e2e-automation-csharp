﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace e2e_automation.tools
{
    class driverTools
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static void WaitForElementToBecomeVisibleWithinTimeout(IWebDriver driver, IWebElement element, int timeout)
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(timeout)).Until(ElementIsVisible(element));
        }

        public static void WaitForElementExistsWithinTimeout(IWebDriver driver, IWebElement element, int timeout, bool traceError = true)
        {
            try
            {
                new WebDriverWait(driver, TimeSpan.FromSeconds(timeout)).Until(ElementExists(element));
            }
            catch (Exception)
            {
                if (traceError)
                {
                    log.Error("The element does not exists after " + timeout + " seconds.");
                }
            }
        }

        private static Func<IWebDriver, bool> ElementIsVisible(IWebElement element)
        {
            return driver => {
                try
                {
                    return element.Displayed;
                }
                catch (Exception)
                {
                    // If element is null, stale or if it cannot be located
                    return false;
                }
            };
        }

        private static Func<IWebDriver, bool> ElementExists(IWebElement element)
        {
            return driver => {
                try
                {
                    return (element == null) ? false : true;

                }
                catch (Exception)
                {
                    // If element is null, stale or if it cannot be located
                    return false;
                }
            };
        }
    }
}
