﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace e2e_automation.tools
{
    class Row
    {
        public List<RowCell> cells = new List<RowCell>();

        public Row()
        {
        }
        public Row(List<RowCell> rowCell)
        {
            cells.AddRange(rowCell);
        }

        public void setCell(RowCell rowCell)
        {
            cells.Add(rowCell);
        }

        public RowCell getActionsCell()
        {

            foreach (RowCell cell in cells)
            {
                if (cell.header.Equals("Actions"))
                {
                    return cell;
                }
            }

            return null;
        }

    }
}
