﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace e2e_automation.tools
{
    public static class Globals
    {
        public static IWebDriver tDriver;
        public static IWebDriver bDriver;
        public static IWebDriver currDriver; //Used as main storage for the required browser (backoffice or terminal)


        //public const int32 buffer_size = 512; // unmodifiable
        //public static string file_name = "output.txt"; // modifiable
        //public static readonly string code_prefix = "us-"; // unmodifiable
    }
}