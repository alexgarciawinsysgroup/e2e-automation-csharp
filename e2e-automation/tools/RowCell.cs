﻿
using HtmlAgilityPack;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace e2e_automation.tools
{
    public class RowCell
    {
        public string header { get; set; }
        public string value { get; set; }
        public IWebElement element { get; set; }

        public RowCell(string header, string value)
        {
            this.header = header;
            this.value = value;
            this.element = null;
        }
        public RowCell(string header, string value, IWebElement element)
        {
            this.header = header;
            this.value = value;
            this.element = element;
        }


    }
}
