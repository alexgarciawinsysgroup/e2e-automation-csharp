﻿using e2e_automation.tools;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace e2e_automation.pages.backoffice.consumables
{
    class bCategoriesPage
    {
        public bCategoriesPage(){}

        //class aria-expanded="true" >> Only appears if submenu its visible.
        private IWebElement lbl_Consumables { get { return Globals.bDriver.FindElement(By.XPath("//*[@href='#consumables-components']")); } }

        private IWebElement lbl_Categories { get { return Globals.bDriver.FindElement(By.XPath("//span[text()='Categories']")); } }

        
        public void goTo()
        {
            lbl_Consumables.Click();
            //System.Threading.Thread.Sleep(500);
            lbl_Categories.Click();
            //System.Threading.Thread.Sleep(1000);
        }

    }
}
