﻿using e2e_automation.tools;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace e2e_automation.pages.backoffice
{
    class bLoginPage
    {
        public bLoginPage() {}

        private IWebElement edf_Username { get { return Globals.bDriver.FindElement(By.Id("username")); } }
        private IWebElement edf_Password { get { return Globals.bDriver.FindElement(By.Id("password")); } }
        private IWebElement btn_Login { get { return Globals.bDriver.FindElement(By.XPath("//*[@type='submit']")); } }

        public void login(string pUser, string pPass)
        {
            edf_Username.SendKeys(pUser);
            edf_Password.SendKeys(pPass);
            btn_Login.Click();
            System.Threading.Thread.Sleep(1500);
        }

        public void openPage()
        {
            Globals.bDriver.Navigate().GoToUrl("http://10.1.11.2:81/pages/login");
            System.Threading.Thread.Sleep(3500);
        }

        public bool isVisible()
        {
            driverTools.WaitForElementExistsWithinTimeout(Globals.bDriver, btn_Login, 3, false);
            return (btn_Login == null) ? false : true;
        }
    }
}
