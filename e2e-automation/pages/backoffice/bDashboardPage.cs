﻿using e2e_automation.tools;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace e2e_automation.pages.backoffice
{
    class bDashboardPage
    {
        public bDashboardPage() {}

        private IWebElement lbl_logo { get { return Globals.bDriver.FindElement(By.XPath("//*[@class='logo']")); } }


        public bool isVisible()
        {
            driverTools.WaitForElementExistsWithinTimeout(Globals.bDriver, lbl_logo, 10, false);
            return (lbl_logo != null);
        }

    }
}
