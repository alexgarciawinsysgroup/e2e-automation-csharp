﻿using e2e_automation.tools;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace e2e_automation.pages.backoffice.common
{
    class bDialogPage
    {
        public bDialogPage() { }

        private IWebElement div_Dialog { get { return Globals.bDriver.FindElement(By.XPath("//div[@role='dialog']")); } }
        private IWebElement img_Type { get { return div_Dialog.FindElement(By.XPath(".//div[@style='display: block;']")); } }
        private IWebElement btn_Continue { get { return div_Dialog.FindElement(By.XPath(".//button[text()='Continue']")); } }
        private IWebElement btn_Cancel { get { return div_Dialog.FindElement(By.XPath(".//button[text()='Cancel']")); } }


        public void clickContinue()
        {
            btn_Continue.Click();
        }

        public void clickCancel()
        {
            btn_Cancel.Click();
        }

        public bool isDialogShown()
        {
            return (div_Dialog != null);
        }

        public string getDialogType()
        {
            string dlgClassAttribute = img_Type.GetAttribute("class");

            string aa = Regex.Replace(dlgClassAttribute, @"(.*)-", "");

            return aa;
        }

        public List<string> isAlertMessageCorrect(string pAlertColor, string pAlertMessage)
        {
            List<string> alertData = new List<string>();
            IWebElement alertContainer = Globals.bDriver.FindElement(By.XPath("//*[@data-notify='container']"));
            if (alertContainer != null)
            {
                IWebElement alertMessage = Globals.bDriver.FindElement(By.XPath("//*[@data-notify='message']"));
                string alertClass = alertContainer.GetAttribute("class");
                string actualAlertColor = (alertClass.Contains("success")) ? "green" : "red";
                alertData.Add(actualAlertColor);
                alertData.Add(alertMessage.GetAttribute("innerText"));
            }

            return alertData;
        }
    }
}
