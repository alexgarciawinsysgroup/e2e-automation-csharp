﻿using e2e_automation.tools;
using HtmlAgilityPack;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace e2e_automation.pages.backoffice.common
{
    class bCommonPage
    {
        public bCommonPage() { }

        private IWebElement btn_Add { get { return Globals.bDriver.FindElement(By.XPath("//button/*[text()='add']/..")); } }
        private IWebElement btn_Search { get { return Globals.bDriver.FindElement(By.XPath("//button/*[text()='search']/..")); } }
        private IWebElement btn_Save { get { return Globals.bDriver.FindElement(By.XPath("//button/*[text()='Save']/..")); } }
        private IWebElement tbl_Table { get { return Globals.bDriver.FindElement(By.Id("datatables")); } }
        
        public void clickAdd()
        {
            btn_Add.Click();
        }

        public void clickSearch()
        {
            btn_Search.Click();
        }

        public void clickSave()
        {
            btn_Save.Click();
        }

        public void clickEditRow(IWebElement row)
        {
            IWebElement btn_EditRow = row.FindElement(By.XPath(".//button/*[text()='edit']/.."));
            btn_EditRow.Click();
        }

        public void clickDeleteRow(IWebElement row)
        {
            IWebElement btn_DeleteRow = row.FindElement(By.XPath(".//button/*[text()='close']/.."));
            btn_DeleteRow.Click();
        }

        public IWebElement fillField(string pFieldId, string pFieldValue)
        {
            IWebElement field = Globals.bDriver.FindElement(By.Id(pFieldId));
            field.Clear();
            field.SendKeys(pFieldValue);

            return field;
        }
                
        //TODO Pagination is not implemented yet. Only works with 10 first values || dataTables_info >> Showing X to X of X entries
        public List<Row> getRows()
        {
            try
            {
                List<Row> dataTable = new List<Row>();
                List<IWebElement> currRowValues = new List<IWebElement>();
                List<IWebElement> currRowHeaders = new List<IWebElement>();

                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(tbl_Table.GetAttribute("innerHTML"));

                //For each 'th' node
                foreach (IWebElement cell in Globals.bDriver.FindElements(By.XPath("//table[@id='datatables']//th")))
                {
                    currRowHeaders.Add(cell);
                }
                //For each 'td' node
                foreach (IWebElement cell in Globals.bDriver.FindElements(By.XPath("//table[@id='datatables']//tr[@class='ng-star-inserted']//td")))
                {
                    currRowValues.Add(cell);
                }

                //Merge header + row value and assign to returnable variable.
                List<RowCell> tableRows = new List<RowCell>();
                for (int i = 0; i < currRowValues.Count; i += currRowHeaders.Count)
                {
                    for (int j = 0; j < currRowHeaders.Count; j++)
                    {
                        //Store each combination of header + value + webElement
                        tableRows.Add(new RowCell(currRowHeaders[j].Text, currRowValues[i + j].Text, currRowValues[i + j]));
                    }
                }

                //Store all cells related from same row to a Row object.
                for (int k = 0; k < (currRowValues.Count / currRowHeaders.Count); k++)
                {
                    List<RowCell> cells = tableRows.GetRange(currRowHeaders.Count * k, currRowHeaders.Count);
                    dataTable.Add(new Row(cells));
                }

                return dataTable;
            }
            catch
            {
                return getRows();
            }
        }

        //Function to determinate if the expected vs actual row objects, is the correct one.
        public bool isExpectedRow(Row expectedList, Row actualList)
        {
            bool rowShown = true;
            foreach (RowCell expectedCell in expectedList.cells)
            {
                bool cellShown = false;
                foreach (RowCell actualCell in actualList.cells)
                {
                    if (expectedCell.header == actualCell.header &&
                            expectedCell.value == actualCell.value)
                    {
                        cellShown = true;
                        break;
                    }
                }
                if (!cellShown)
                {
                    rowShown = false;
                    break;
                }
            }

            return rowShown;
        }



    }
}
