﻿using e2e_automation.tools;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace e2e_automation.pages.terminal
{
    class tDashboardPage
    {
        public tDashboardPage() { }

        private IWebElement div_Games { get { return Globals.tDriver.FindElement(By.XPath("//app-games")); } }


        public bool isVisible()
        {
            driverTools.WaitForElementExistsWithinTimeout(Globals.bDriver, div_Games, 10, false);
            return (div_Games != null);
        }

        public void goToGame(string pGame)
        {

        }
    }
}
