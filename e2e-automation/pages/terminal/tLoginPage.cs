﻿using e2e_automation.tools;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace e2e_automation.pages.terminal
{
    class tLoginPage
    {
        private IWebDriver driver;

        private IWebElement btn_0 { get { return Globals.tDriver.FindElement(By.XPath("//button[text()='0']")); } }
        private IWebElement btn_1 { get { return Globals.tDriver.FindElement(By.XPath("//button[text()='1']")); } }
        private IWebElement btn_2 { get { return Globals.tDriver.FindElement(By.XPath("//button[text()='2']")); } }
        private IWebElement btn_3 { get { return Globals.tDriver.FindElement(By.XPath("//button[text()='3']")); } }
        private IWebElement btn_4 { get { return Globals.tDriver.FindElement(By.XPath("//button[text()='4']")); } }
        private IWebElement btn_5 { get { return Globals.tDriver.FindElement(By.XPath("//button[text()='5']")); } }
        private IWebElement btn_6 { get { return Globals.tDriver.FindElement(By.XPath("//button[text()='6']")); } }
        private IWebElement btn_7 { get { return Globals.tDriver.FindElement(By.XPath("//button[text()='7']")); } }
        private IWebElement btn_8 { get { return Globals.tDriver.FindElement(By.XPath("//button[text()='8']")); } }
        private IWebElement btn_9 { get { return Globals.tDriver.FindElement(By.XPath("//button[text()='9']")); } }
        private IWebElement txt_TellerId { get { return Globals.tDriver.FindElement(By.Id("input-id")); } }
        private IWebElement txt_Password { get { return Globals.tDriver.FindElement(By.Id("pin")); } }
        private IWebElement btn_Login { get { return Globals.tDriver.FindElement(By.XPath("//*[@type='submit']")); } }
        private IWebElement btn_TrainingMode { get { return Globals.tDriver.FindElement(By.XPath("//*[text()='TRAINING MODE']")); } }

        public tLoginPage() { }

        public void openPage()
        {
            Globals.tDriver.Navigate().GoToUrl("localhost:4200");
        }

        public void login(string user, string pass, bool useNumPad = true)
        {
            if (useNumPad)
            {
                //User
                for (int i = 0; i <= user.Length - 1; i++)
                {
                    selectNumberNumPad(Int32.Parse(user[i].ToString()));
                }

                //Password
                for (int j = 0; j <= pass.Length - 1; j++)
                {
                    selectNumberNumPad(Int32.Parse(pass[j].ToString()));
                }

            }
            else
            {
                txt_TellerId.SendKeys(user);
                txt_Password.SendKeys(pass);
            }


            btn_Login.Click();
        }

        //Select the number from the numpad 
        private void selectNumberNumPad(int num)
        {
            switch (num)
            {
                case 0:
                    btn_0.Click();
                    break;
                case 1:
                    btn_1.Click();
                    break;
                case 2:
                    btn_2.Click();
                    break;
                case 3:
                    btn_3.Click();
                    break;
                case 4:
                    btn_4.Click();
                    break;
                case 5:
                    btn_5.Click();
                    break;
                case 6:
                    btn_6.Click();
                    break;
                case 7:
                    btn_7.Click();
                    break;
                case 8:
                    btn_8.Click();
                    break;
                case 9:
                    btn_9.Click();
                    break;
                default:
                    Console.WriteLine("Invalid value. Values must be from 0 to 9.");
                    break;
            }
        }
    }
}
